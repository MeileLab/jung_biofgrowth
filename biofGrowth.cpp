// lattice boltzmann simulation for biofilm growth
// the paper submitted to Transport in Porous Media
// Jung and Meile (2020) Pore scale numerical investigation of evolving porosity and permeability driven by biofilm growth
// Palabos coupled to SUNDIALS cvode             written by Heewon Jung (hjung.work@gmail.com)

#include "palabos2D.h"
#include "palabos2D.hh"
#include <vector>
#include <cmath>
#include <cstdlib>
#include <sys/stat.h>
#include <ctime>
#include <stack>

#include "biofGrowth.h"

using namespace plb;
typedef double T;

#define NSDES descriptors::D2Q9Descriptor // Cs2 = 1/3
#define RXNDES descriptors::AdvectionDiffusionD2Q5Descriptor // Cs2 = 1/3

bool fexists(const char *filename) {
    plb_ifstream checkfile(filename);
    if (checkfile.is_open()) {
        return 1;
        checkfile.close();
    }
    else {
        return 0;
    }
}

class PressureGradient {
public:
    PressureGradient(T deltaP_, plint nx_) : deltaP(deltaP_), nx(nx_)
    { }
    void operator() (plint iX, plint iY, T& density, Array<T,2>& velocity) const
    {
        velocity.resetToZero();
        density = (T)1 - deltaP*NSDES<T>::invCs2 / (T)(nx-1) * (T)iX;
    }
private:
    T deltaP;
    plint nx;
};

void writeNsVTK(MultiBlockLattice2D<T,NSDES>& lattice, plint iter, std::string nameid)
{
    VtkImageOutput2D<T> vtkOut(createFileName("vtkVelocity" + nameid, iter, 7), 1.);

    vtkOut.writeData<float>(*computeVelocityNorm(lattice), "velocityNorm", 1.);
    vtkOut.writeData<2,float>(*computeVelocity(lattice), "velocity", 1.);
    vtkOut.writeData<float>(*computeDensity(lattice), "Density", 1.);
}

void writePorousMediumVTK(MultiScalarField2D<int>& geometry, plint iter)
{
    VtkImageOutput2D<T> vtkOut(createFileName("porousMedium", iter, 7), 1.);
    vtkOut.writeData<float>(*copyConvert<int,T>(geometry, geometry.getBoundingBox()), "tag", 1.0);
}

void writeAdvVTK(MultiBlockLattice2D<T,RXNDES>& lattice, plint iter, std::string nameid)
{
    VtkImageOutput2D<T> vtkOut(createFileName("vtk" + nameid, iter, 7), 1.);
    vtkOut.writeData<T>(*computeDensity(lattice), "Density", 1.);
}

void readGeometry(std::string fNameIn, MultiScalarField2D<int>& geometry)
{

    pcout << "Reading the geometry file." << std::endl;
    const plint nx = geometry.getNx();
    const plint ny = geometry.getNy();

    Box2D sliceBox(0,0, 0,ny-1);
    std::auto_ptr<MultiScalarField2D<int> > slice = generateMultiScalarField<int>(geometry, sliceBox);

    plb_ifstream geometryFile(fNameIn.c_str());
    for (plint iX=0; iX<nx; ++iX) {
        if (!geometryFile.is_open()) {
            pcout << "Error: could not open geometry file " << fNameIn << std::endl;
            exit(EXIT_FAILURE);
        }
        geometryFile >> *slice;
        copy(*slice, slice->getBoundingBox(), geometry, Box2D(iX,iX, 0,ny-1));
    }

    {
        VtkImageOutput2D<T> vtkOut("porousMedium", 1.0);
        vtkOut.writeData<float>(*copyConvert<int,T>(geometry, geometry.getBoundingBox()), "tag", 1.0);
    }
}

void NSdomainSetup(MultiBlockLattice2D<T,NSDES>& lattice,
                    OnLatticeBoundaryCondition2D<T,NSDES>* boundaryCondition,
                    MultiScalarField2D<int>& geometry, T deltaP, T fluidOmega)
{
    const plint nx = lattice.getNx();
    const plint ny = lattice.getNy();

    Box2D west   (0,0, 0,ny-1);
    Box2D east   (nx-1,nx-1, 0,ny-1);

    defineDynamics(lattice, lattice.getBoundingBox(), new IncBGKdynamics<T,NSDES>(fluidOmega));
    defineDynamics(lattice, geometry, new NoDynamics<T,NSDES>(), 0);
    defineDynamics(lattice, geometry, new BounceBack<T,NSDES>(), 1);
    // uncomment the following 3 lines for permeable biofilm simulations 
    // T X = 3.; // viscosity ratio, either 3 or 30 for high or low biofilm permeability
    // T bfomega = 1/(X*(1/fluidOmega-.5)+.5);
    // defineDynamics(lattice, geometry, new IncBGKdynamics<T,NSDES>(bfomega), 1);

    boundaryCondition->addPressureBoundary0N(west, lattice);
    setBoundaryDensity(lattice, west, (T) 1.);

    boundaryCondition->addPressureBoundary0P(east, lattice);
    setBoundaryDensity(lattice, east, (T) 1. - deltaP*NSDES<T>::invCs2);

    initializeAtEquilibrium(lattice, lattice.getBoundingBox(), PressureGradient(deltaP, nx));
    
    lattice.initialize();
    delete boundaryCondition;
}

void solute1DomainSetup(MultiBlockLattice2D<T,RXNDES> &lattice,
                        OnLatticeAdvectionDiffusionBoundaryCondition2D<T, RXNDES>* boundaryCondition,
                        MultiScalarField2D<int>& geometry, T rho0, T rhoInlet, T bMassOmega, T soluteOmega)
{
    const plint nx = lattice.getNx();
    const plint ny = lattice.getNy();

    Box2D west   (0,0, 0,ny-1);
    Box2D east   (nx-1,nx-1, 0,ny-1);
    plint processorLevelBC = 1;
  
    defineDynamics(lattice, lattice.getBoundingBox(), new AdvectionDiffusionBGKdynamics<T,RXNDES>(soluteOmega));
    defineDynamics(lattice, geometry, new NoDynamics<T,RXNDES>(), 0);
    defineDynamics(lattice, geometry, new BounceBack<T,RXNDES>(), 1);
    defineDynamics(lattice, geometry, new AdvectionDiffusionBGKdynamics<T,RXNDES>(bMassOmega), 2);

    // Set the boundary-conditions
    boundaryCondition->addTemperatureBoundary0P(east, lattice);
    integrateProcessingFunctional(new FlatAdiabaticBoundaryFunctional2D<T,RXNDES, 0, +1>, east, lattice, processorLevelBC);
    
    boundaryCondition->addTemperatureBoundary0N(west, lattice);
    setBoundaryDensity(lattice, west, rhoInlet);

    // lattice.periodicity().toggle(1, true);

    // Init lattice
    Array<T,2> u0(0., 0.);
    initializeAtEquilibrium(lattice, lattice.getBoundingBox(), rho0, u0);
    
    lattice.initialize();
    delete boundaryCondition;
}

void scalarDomainSetup(MultiBlockLattice2D<T,RXNDES> &lattice, MultiScalarField2D<int>& geometry, T bMass0, T latticeOmega)
{
    defineDynamics(lattice, lattice.getBoundingBox(), new AdvectionDiffusionBGKdynamics<T,RXNDES>(latticeOmega));
    defineDynamics(lattice, geometry, new BounceBack<T,RXNDES>(), 1);
    defineDynamics(lattice, geometry, new NoDynamics<T,RXNDES>(), 0);

    // Init lattice
    Array<T,2> jEq(0., 0.);
    initializeAtEquilibrium(lattice, lattice.getBoundingBox(), bMass0, jEq);

    lattice.initialize();
}
/* =============================================================================================================== 
   =============================================== DATA PROCESSORS =============================================== 
   =============================================================================================================== */

// nonlocal biomass reaction
template<typename T, template<typename U> class Descriptor>
class localReactionLattices2D : public LatticeBoxProcessingFunctional2D<T,Descriptor>
{
public:
    localReactionLattices2D(T dt_) : dt(dt_)
    {}
    // lattices[0] = solute 1 concentration
    // lattices[1] = surface-associated biomass 1
    // lattices[2] = mask field lattice
    virtual void process(Box2D domain, std::vector<BlockLattice2D<T,Descriptor>*> lattices) {
        Dot2D offset_01 = computeRelativeDisplacement(*lattices[0],*lattices[1]);
        Dot2D offset_02 = computeRelativeDisplacement(*lattices[0],*lattices[2]);
        for (plint iX0=domain.x0; iX0<=domain.x1; ++iX0) {
            plint iX2 = iX0 + offset_02.x;
            for (plint iY0=domain.y0; iY0<=domain.y1; ++iY0) {
                plint iY2 = iY0 + offset_02.y;
                plint mask = std::round( lattices[2]->get(iX2,iY2).computeDensity() );
                if ( (mask > 1 && mask < 5) || mask > 6 ) { // biomass
                    Array<T,5> g;
                    plint iX1 = iX0 + offset_01.x; plint iY1 = iY0 + offset_01.y;

                    T c0 = lattices[0]->get(iX0,iY0).computeDensity(); // (mM)
                    T B2 = lattices[1]->get(iX1,iY1).computeDensity(); // sessile (gdw/L)

                    call_biomassGrowth(c0, B2, dt);

                    lattices[0]->get(iX0,iY0).getPopulations(g);
                    g[0]+=(T) c0/3; g[1]+=(T) c0/6; g[2]+=(T) c0/6; g[3]+=(T) c0/6; g[4]+=(T) c0/6;
                    lattices[0]->get(iX0,iY0).setPopulations(g);

                    lattices[1]->get(iX1,iY1).getPopulations(g);
                    g[0]+=(T) B2/3; g[1]+=(T) B2/6; g[2]+=(T) B2/6; g[3]+=(T) B2/6; g[4]+=(T) B2/6;
                    lattices[1]->get(iX1,iY1).setPopulations(g);
                }
            }
        }
    }
    virtual BlockDomain::DomainT appliesTo() const {
        return BlockDomain::bulkAndEnvelope;
    }
    virtual localReactionLattices2D<T,Descriptor>* clone() const {
        return new localReactionLattices2D<T,Descriptor>(*this);
    }
    void getTypeOfModification(std::vector<modif::ModifT>& modified) const {
        modified[0] = modif::staticVariables;
        modified[1] = modif::staticVariables;
        modified[2] = modif::nothing;
    }
private:
    T dt;
};

// redistribute the excessive biomass (push)
template<typename T, template<typename U> class Descriptor>
class pushExcessBiomass2D : public LatticeBoxProcessingFunctional2D<T,Descriptor>
{
public:
    pushExcessBiomass2D(T Bmax_, plint nx_, plint ny_, plint bdryGap_)
    : Bmax(Bmax_), nx(nx_), ny(ny_), bdryGap(bdryGap_)
    {}
    // lattices[0] = surface-associated biomass 1
    // lattices[1] = mask field lattice
    // lattices[2] = storage lattice
    // lattices[3] = bdry direction lattice
    // lattices[4] = mask2 field lattice
    virtual void process(Box2D domain, std::vector<BlockLattice2D<T,Descriptor>*> lattices) {
        Dot2D offset_01 = computeRelativeDisplacement(*lattices[0],*lattices[1]);
        Dot2D offset_02 = computeRelativeDisplacement(*lattices[0],*lattices[2]);
        Dot2D offset_03 = computeRelativeDisplacement(*lattices[0],*lattices[3]);
        Dot2D offset_04 = computeRelativeDisplacement(*lattices[0],*lattices[4]);
        Dot2D offset_05 = computeRelativeDisplacement(*lattices[0],*lattices[5]);
        Dot2D absoluteOffset = lattices[0]->getLocation();
        for (plint iX0=domain.x0; iX0<=domain.x1; ++iX0) {
            plint iX1 = iX0 + offset_01.x; plint absX = iX0 + absoluteOffset.x;
            for (plint iY0=domain.y0; iY0<=domain.y1; ++iY0) {
                plint iY1 = iY0 + offset_01.y; plint absY = iY0 + absoluteOffset.y;
                plint mask = std::round( lattices[1]->get(iX1,iY1).computeDensity() );
                if ( (mask > 1 && mask < 5) || mask > 6 ) {
                    T B2 = lattices[0]->get(iX0,iY0).computeDensity();
                    T B2delta = B2-Bmax;
                    // check if the local biomass exceeded the threshold
                    if ( B2delta > 0 ) {
                        std::vector<plint> delXY; plint len = 0;
                        if ( absX == bdryGap && absY > 0 && absY < (ny-1) ) {
                            delXY.push_back(1); delXY.push_back(0);
                            delXY.push_back(0); delXY.push_back(1);
                            delXY.push_back(0); delXY.push_back(-1);
                            // len is the number of neighbors depending on the current location
                            len = 3;
                        }
                        else if ( absX == (nx-1-bdryGap) && absY > 0 && absY < (ny-1) ) {
                            delXY.push_back(-1); delXY.push_back(0);
                            delXY.push_back(0); delXY.push_back(1);
                            delXY.push_back(0); delXY.push_back(-1);
                            len = 3;
                        }
                        else if ( absX > bdryGap && absX < (nx-1-bdryGap) && absY == 0 ) {
                            delXY.push_back(-1); delXY.push_back(0);
                            delXY.push_back(1); delXY.push_back(0);
                            delXY.push_back(0); delXY.push_back(1);
                            len = 3;
                        }
                        else if ( absX > bdryGap && absX < (nx-1-bdryGap) && absY == (ny-1) ) {
                            delXY.push_back(-1); delXY.push_back(0);
                            delXY.push_back(1); delXY.push_back(0);
                            delXY.push_back(0); delXY.push_back(-1);
                            len = 3;
                        }
                        else if ( absX == bdryGap && absY == 0 ) {
                            delXY.push_back(1); delXY.push_back(0);
                            delXY.push_back(0); delXY.push_back(1);
                            len = 2;
                        }
                        else if ( absX == bdryGap && absY == (ny-1) ) {
                            delXY.push_back(1); delXY.push_back(0);
                            delXY.push_back(0); delXY.push_back(-1);
                            len = 2;
                        }
                        else if ( absX == (nx-1-bdryGap) && absY == 0 ) {
                            delXY.push_back(-1); delXY.push_back(0);
                            delXY.push_back(0); delXY.push_back(1);
                            len = 2;
                        }
                        else if ( absX == (nx-1-bdryGap) && absY == ny-1 ) {
                            delXY.push_back(-1); delXY.push_back(0);
                            delXY.push_back(0); delXY.push_back(-1);
                            len = 2;
                        }
                        else {
                            delXY.push_back(1); delXY.push_back(0);
                            delXY.push_back(-1); delXY.push_back(0);
                            delXY.push_back(0); delXY.push_back(1);
                            delXY.push_back(0); delXY.push_back(-1);
                            len = 4;
                        }
                        // check if iX or iY is located on the subdomain boundaries (MPI)
                        plint curLoc = 0;
                        if ( absX > bdryGap && absX < (nx-1-bdryGap) && absY > 0 && absY < (ny-1) ) {
                            if (iX0 == domain.x1 && iY0 > domain.y0 && iY0 < domain.y1) { curLoc = 1; }
                            else if (iY0 == domain.y1 && iX0 > domain.x0 && iX0 < domain.x1) { curLoc = 2; }
                            else if (iX0 == domain.x0 && iY0 > domain.y0 && iY0 < domain.y1)  { curLoc = 3; }
                            else if (iY0 == domain.y0 && iX0 > domain.x0 && iX0 < domain.x1) { curLoc = 4; }
                            else if (iX0 == domain.x1 && iY0 == domain.y1) { curLoc = 5; }
                            else if (iX0 == domain.x0 && iY0 == domain.y1) { curLoc = 6; }
                            else if (iX0 == domain.x0 && iY0 == domain.y0) { curLoc = 7; }
                            else if (iX0 == domain.x1 && iY0 == domain.y0) { curLoc = 8; }
                        }
                        // search neighbors for redistribution
                        std::vector<plint> collectiveVector; plint lenT = 0;
                        for (plint iT=0; iT<len; ++iT) {
                            plint delx = delXY[iT*2], dely = delXY[iT*2+1];
                            plint tmpMask = std::round( lattices[1]->get(iX1+delx,iY1+dely).computeDensity() );
                            // exclude wall boundaries
                            if (tmpMask != 1) {
                                collectiveVector.push_back(delx);
                                collectiveVector.push_back(dely);
                                collectiveVector.push_back(tmpMask);
                                ++lenT;
                            }
                        }
                        // shuffle a number vector to randomly select a neighbor
                        std::vector<plint> randArray;
                        if (lenT > 1) {
                            for (plint iR=0; iR<lenT; ++iR) {
                                randArray.push_back(iR);
                            }
                            std::random_shuffle(randArray.begin(), randArray.end());
                        }
                        // main loop
                        bool marker = 0;
                        if (lenT > 0) {
                            for (plint iT=0; iT<lenT; ++iT) {
                                plint iR = 0;
                                if (lenT > 1) {
                                    iR = randArray[iT];
                                }
                                plint delx = collectiveVector[3*iR]; plint dely = collectiveVector[3*iR+1]; plint tmpMask = collectiveVector[3*iR+2];
                                plint tmpLoc = 0;
                                if (curLoc > 0) {
                                    if ( delx == 1 && (curLoc == 1 || curLoc == 5 || curLoc == 8) ) { tmpLoc = 1; }
                                    else if ( dely == 1 && (curLoc == 2 || curLoc == 5 || curLoc == 6) ) { tmpLoc = 2; }
                                    else if ( delx == -1 && (curLoc == 3 || curLoc == 6 || curLoc == 7) ) { tmpLoc = 3; }
                                    else if ( dely == -1 && (curLoc == 4 || curLoc == 7 || curLoc == 8) ) { tmpLoc = 4; }
                                    else { tmpLoc = 0; }
                                }
                                T tmpB2;
                                if (tmpMask == 0 || tmpMask == 6) {
                                    tmpB2 = 0; // to get rid of an artificially small number of B2 at pore spaces
                                }
                                else {
                                    tmpB2 = lattices[0]->get(iX0+delx,iY0+dely).computeDensity();
                                }
                                if (tmpB2 < Bmax) {
                                    B2delta += tmpB2;
                                    Array<T,5> g;
                                    // set the updated biomass density for the selected neighboring voxel
                                    if (B2delta > Bmax) {
                                        g[0]=(T) (Bmax-1)/3; g[1]=g[2]=g[3]=g[4]=(T) (Bmax-1)/6;
                                        B2delta -= Bmax;
                                    }
                                    else {
                                        g[0]=(T) (B2delta-1)/3; g[1]=g[2]=g[3]=g[4]=(T) (B2delta-1)/6;
                                        B2delta = 0;
                                        marker = 1;
                                    }
                                    // If the selected neighbor location is ouside the MPI boundary, update the storage lattice (lattice2) to be pulled instead of pushed
                                    if ( tmpLoc > 0) {
                                        plint iX2 = iX0+offset_02.x; plint iY2 = iY0+offset_02.y;
                                        plint iX3 = iX0+offset_03.x; plint iY3 = iY0+offset_03.y;
                                        lattices[2]->get(iX2,iY2).setPopulations(g); // update the local lattice population of the storage lattice
                                        g[0]=(T) (tmpLoc-1)/3; g[1]=g[2]=g[3]=g[4]=(T) (tmpLoc-1)/6;
                                        lattices[3]->get(iX3,iY3).setPopulations(g); // update the indicator lattice for direction
                                    }
                                    else {
                                        lattices[0]->get(iX0+delx,iY0+dely).setPopulations(g);
                                        if (tmpMask == 0) {
                                            g[0]=(T) (3-1)/3; g[1]=g[2]=g[3]=g[4]=(T) (3-1)/6;
                                            lattices[1]->get(iX1+delx,iY1+dely).setPopulations(g); // update the neighbor mask number from 0 to 3 for biomass expansion
                                        }
                                        else if (tmpMask == 6) {
                                            g[0]=(T) (7-1)/3; g[1]=g[2]=g[3]=g[4]=(T) (7-1)/6;
                                            lattices[1]->get(iX1+delx,iY1+dely).setPopulations(g); // update the neighbor mask number from 6 to 7 for biomass expansion
                                        }
                                    }
                                    // break out of the loop if the excess biomass has been redistributed 
                                    if (marker == 1) {
                                        break;
                                    }
                                }
                            }
                        }
                        // update the local biomass density after the initial redistibution to a closest neighbor
                        Array<T,5> g;
                        g[0]=(T) (Bmax-1)/3; g[1]=g[2]=g[3]=g[4]=(T) (Bmax-1)/6;
                        lattices[0]->get(iX0,iY0).setPopulations(g);
                        
                        // push the remaining biomass
                        if (marker == 0) {
                            // use the pre-allocated mask number (lattice4)
                            std::vector<T> tmp1Vector, tmp2Vector;
                            plint tmp1Len = 0, tmp2Len = 0, delx = 0, dely = 0, tmpMask = 0;
                            plint iX4 = iX0+offset_04.x, iY4 = iY0+offset_04.y;
                            plint id0 = std::round( lattices[4]->get(iX4,iY4).computeDensity() );
                            for (plint iT=0; iT<lenT; ++iT) {
                                delx = collectiveVector[iT*3]; dely = collectiveVector[iT*3+1]; tmpMask = collectiveVector[iT*3+2];
                                plint id1 = std::round( lattices[4]->get(iX4+delx,iY4+dely).computeDensity() );
                                if ( id0 <= 18 ) {
                                    if ( id0 <= id1 ) {
                                        tmp1Vector.push_back(delx);
                                        tmp1Vector.push_back(dely);
                                        tmp1Vector.push_back(tmpMask);
                                        ++tmp1Len;
                                    }
                                }
                                else if ( id1 >= 19 ) {
                                    tmp2Vector.push_back(delx);
                                    tmp2Vector.push_back(dely);
                                    tmp2Vector.push_back(tmpMask);
                                    ++tmp2Len;
                                }
                            }
                            if ( tmp1Len > 1) {
                                plint randLoc = std::rand() % tmp1Len;
                                delx = tmp1Vector[randLoc*3]; dely = tmp1Vector[randLoc*3+1]; tmpMask = tmp1Vector[randLoc*3+2];
                            }
                            else if (tmp1Len == 1) {
                                delx = tmp1Vector[0]; dely = tmp1Vector[1]; tmpMask = tmp1Vector[2];
                            }
                            else { // tmp1Len < 1
                                if ( tmp2Len > 1) {
                                    plint randLoc = std::rand() % tmp2Len;
                                    delx = tmp2Vector[randLoc*3]; dely = tmp2Vector[randLoc*3+1]; tmpMask = tmp2Vector[randLoc*3+2];
                                }
                                else if (tmp2Len == 1) {
                                    delx = tmp2Vector[0]; dely = tmp2Vector[1]; tmpMask = tmp2Vector[2];
                                }
                                else { // tmp2Len < 1
                                    std::cout << "biomass push does not work.\n";
                                    exit(EXIT_FAILURE);
                                }
                            }

                            if ( (tmpMask > 1 && tmpMask < 5) || tmpMask > 6 ) {
                                T tmpB2 = lattices[0]->get(iX0+delx,iY0+dely).computeDensity();
                                g[0]=(T) (tmpB2+B2delta-1)/3; g[1]=g[2]=g[3]=g[4]=(T) (tmpB2+B2delta-1)/6;
                                if (curLoc > 0) {
                                    if ( delx == 1 && (curLoc == 1 || curLoc == 5 || curLoc == 8) ) { curLoc = 1; }
                                    else if ( dely == 1 && (curLoc == 2 || curLoc == 5 || curLoc == 6) ) { curLoc = 2; }
                                    else if ( delx == -1 && (curLoc == 3 || curLoc == 6 || curLoc == 7) ) { curLoc = 3; }
                                    else if ( dely == -1 && (curLoc == 4 || curLoc == 7 || curLoc == 8) ) { curLoc = 4; }
                                    else { curLoc = 0; }
                                }
                                if (curLoc > 0) {
                                    plint iX2 = iX0+offset_02.x; plint iY2 = iY0+offset_02.y;
                                    plint iX3 = iX0+offset_03.x; plint iY3 = iY0+offset_03.y;
                                    lattices[2]->get(iX2,iY2).setPopulations(g); // update the local lattice population of the storage lattice
                                    g[0]=(T) (curLoc-1)/3; g[1]=g[2]=g[3]=g[4]=(T) (curLoc-1)/6;
                                    lattices[3]->get(iX3,iY3).setPopulations(g); // update the indicator lattice for direction
                                }
                                else {
                                    lattices[0]->get(iX0+delx,iY0+dely).setPopulations(g);
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    virtual BlockDomain::DomainT appliesTo() const {
        // Don't apply to envelope, because nearest neighbors need to be accessed.
        return BlockDomain::bulk;
    }
    virtual pushExcessBiomass2D<T,Descriptor>* clone() const {
        return new pushExcessBiomass2D<T,Descriptor>(*this);
    }
    void getTypeOfModification(std::vector<modif::ModifT>& modified) const {
        modified[0] = modif::staticVariables;
        modified[1] = modif::staticVariables;
        modified[2] = modif::staticVariables;
        modified[3] = modif::staticVariables;
        modified[4] = modif::nothing;
        modified[5] = modif::nothing;
    }
private:
    T Bmax;
    plint nx;
    plint ny;
    plint bdryGap;
};

// redistribute the excessive biomass (pull)
template<typename T, template<typename U> class Descriptor>
class pullInterfaceBiomass2D : public LatticeBoxProcessingFunctional2D<T,Descriptor>
{
public:
    pullInterfaceBiomass2D(plint nx_, plint ny_, plint bdryGap_)
    : nx(nx_), ny(ny_), bdryGap(bdryGap_)
    {}
    // lattices[0] = surface-associated biomass 1
    // lattices[1] = mask field lattice
    // lattices[2] = storage lattice
    // lattices[3] = bdry direction lattice
    // (not used) lattices[4] = solute 1 concentration
    // (not used) lattices[5] = indicator lattice
    virtual void process(Box2D domain, std::vector<BlockLattice2D<T,Descriptor>*> lattices) {
        Dot2D offset_01 = computeRelativeDisplacement(*lattices[0],*lattices[1]);
        Dot2D offset_02 = computeRelativeDisplacement(*lattices[0],*lattices[2]);
        Dot2D offset_03 = computeRelativeDisplacement(*lattices[0],*lattices[3]);
        Dot2D absoluteOffset = lattices[0]->getLocation();
        plint iX0 = domain.x0; plint iX3 = iX0 + offset_03.x; plint absX = iX0 + absoluteOffset.x;
        if ( absX >= bdryGap ) {
            for (plint iY0=domain.y0; iY0<=domain.y1; ++iY0) {
                plint iY3 = iY0 + offset_03.y;
                plint tmpLoc = std::round( lattices[3]->get((iX3-1),iY3).computeDensity() );
                if (tmpLoc == 1) {
                    Array<T,5> g;
                    plint iX2 = iX0 + offset_02.x, iY2 = iY0 + offset_02.y;
                    T B2 = lattices[2]->get((iX2-1),iY2).computeDensity();
                    if (B2 > 0) {
                        plint iX1 = iX0 + offset_01.x, iY1 = iY0 + offset_01.y;
                        plint mask = std::round( lattices[1]->get(iX1,iY1).computeDensity() );
                        g[0]=(T) (B2-1)/3; g[1]=g[2]=g[3]=g[4]=(T) (B2-1)/6;
                        lattices[0]->get(iX0,iY0).setPopulations(g);
                        if (mask == 0) {
                            g[0]=(T) (3-1)/3; g[1]=g[2]=g[3]=g[4]=(T) (3-1)/6;
                            lattices[1]->get(iX1,iY1).setPopulations(g); // update the neighbor mask number from 0 to 3 for biomass expansion
                        }
                        else if (mask == 6) {
                            g[0]=(T) (7-1)/3; g[1]=g[2]=g[3]=g[4]=(T) (7-1)/6;
                            lattices[1]->get(iX1,iY1).setPopulations(g); // update the neighbor mask number from 6 to 7 for biomass expansion
                        }
                    }
                }
            }
        }
        iX0 = domain.x1; iX3 = iX0 + offset_03.x; absX = iX0 + absoluteOffset.x;
        if ( absX <= (nx-1-bdryGap) ) {
            for (plint iY0=domain.y0; iY0<=domain.y1; ++iY0) {
                plint iY3 = iY0 + offset_03.y;
                plint tmpLoc = std::round( lattices[3]->get((iX3+1),iY3).computeDensity() );
                if (tmpLoc == 3) {
                    Array<T,5> g;
                    plint iX2 = iX0 + offset_02.x, iY2 = iY0 + offset_02.y;
                    T B2 = lattices[2]->get((iX2+1),iY2).computeDensity();
                    if (B2 > 0) {
                        plint iX1 = iX0 + offset_01.x, iY1 = iY0 + offset_01.y;
                        plint mask = std::round( lattices[1]->get(iX1,iY1).computeDensity() );
                        g[0]=(T) (B2-1)/3; g[1]=g[2]=g[3]=g[4]=(T) (B2-1)/6;
                        lattices[0]->get(iX0,iY0).setPopulations(g);
                        if (mask == 0) {
                            g[0]=(T) (3-1)/3; g[1]=g[2]=g[3]=g[4]=(T) (3-1)/6;
                            lattices[1]->get(iX1,iY1).setPopulations(g); // update the neighbor mask number from 0 to 3 for biomass expansion
                        }
                        else if (mask == 6) {
                            g[0]=(T) (7-1)/3; g[1]=g[2]=g[3]=g[4]=(T) (7-1)/6;
                            lattices[1]->get(iX1,iY1).setPopulations(g); // update the neighbor mask number from 6 to 7 for biomass expansion
                        }
                    }
                }
            }
        }
        plint iY0 = domain.y0; plint iY3 = iY0 + offset_03.y; plint absY = iY0 + absoluteOffset.y;
        if ( absY > 0 ) {
            for (iX0=domain.x0; iX0<=domain.x1; ++iX0) {
                iX3 = iX0 + offset_03.x;
                plint tmpLoc = std::round( lattices[3]->get(iX3,(iY3-1)).computeDensity() );
                if (tmpLoc == 2) {
                    Array<T,5> g;
                    plint iX2 = iX0 + offset_02.x, iY2 = iY0 + offset_02.y;
                    T B2 = lattices[2]->get(iX2,(iY2-1)).computeDensity();
                    if (B2 > 0) {
                        plint iX1 = iX0 + offset_01.x, iY1 = iY0 + offset_01.y;
                        plint mask = std::round( lattices[1]->get(iX1,iY1).computeDensity() );
                        g[0]=(T) (B2-1)/3; g[1]=g[2]=g[3]=g[4]=(T) (B2-1)/6;
                        lattices[0]->get(iX0,iY0).setPopulations(g);
                        if (mask == 0) {
                            g[0]=(T) (3-1)/3; g[1]=g[2]=g[3]=g[4]=(T) (3-1)/6;
                            lattices[1]->get(iX1,iY1).setPopulations(g); // update the neighbor mask number from 0 to 3 for biomass expansion
                        }
                        else if (mask == 6) {
                            g[0]=(T) (7-1)/3; g[1]=g[2]=g[3]=g[4]=(T) (7-1)/6;
                            lattices[1]->get(iX1,iY1).setPopulations(g); // update the neighbor mask number from 6 to 7 for biomass expansion
                        }
                    }
                }
            }
        }
        iY0 = domain.y1; iY3 = iY0 + offset_03.y; absY = iY0 + absoluteOffset.y;
        if ( absY < (ny-1) ) {
            for (iX0=domain.x0; iX0<=domain.x1; ++iX0) {
                iX3 = iX0 + offset_03.x;
                plint tmpLoc = std::round( lattices[3]->get(iX3,(iY3+1)).computeDensity() );
                if (tmpLoc == 4) {
                    Array<T,5> g;
                    plint iX2 = iX0 + offset_02.x, iY2 = iY0 + offset_02.y;
                    T B2 = lattices[2]->get(iX2,(iY2+1)).computeDensity();
                    if (B2 > 0) {
                        plint iX1 = iX0 + offset_01.x, iY1 = iY0 + offset_01.y;
                        plint mask = std::round( lattices[1]->get(iX1,iY1).computeDensity() );
                        g[0]=(T) (B2-1)/3; g[1]=g[2]=g[3]=g[4]=(T) (B2-1)/6;
                        lattices[0]->get(iX0,iY0).setPopulations(g);
                        if (mask == 0) {
                            g[0]=(T) (3-1)/3; g[1]=g[2]=g[3]=g[4]=(T) (3-1)/6;
                            lattices[1]->get(iX1,iY1).setPopulations(g); // update the neighbor mask number from 0 to 3 for biomass expansion
                        }
                        else if (mask == 6) {
                            g[0]=(T) (7-1)/3; g[1]=g[2]=g[3]=g[4]=(T) (7-1)/6;
                            lattices[1]->get(iX1,iY1).setPopulations(g); // update the neighbor mask number from 6 to 7 for biomass expansion
                        }
                    }
                }
            }
        }
    }
    virtual BlockDomain::DomainT appliesTo() const {
        // Don't apply to envelope, because nearest neighbors need to be accessed.
        return BlockDomain::bulk;
    }
    virtual pullInterfaceBiomass2D<T,Descriptor>* clone() const {
        return new pullInterfaceBiomass2D<T,Descriptor>(*this);
    }
    void getTypeOfModification(std::vector<modif::ModifT>& modified) const {
        modified[0] = modif::staticVariables;
        modified[1] = modif::staticVariables;
        modified[2] = modif::nothing;
        modified[3] = modif::nothing;
    }
private:
    plint nx;
    plint ny;
    plint bdryGap;
};

// Update nonlocal mask number at every timestep
template<typename T, template<typename U> class Descriptor>
class nonlocalMaskNumberUpdate2D : public BoxProcessingFunctional2D_L<T,Descriptor>
{
public:
    nonlocalMaskNumberUpdate2D(plint nx_, plint ny_)
    : nx(nx_), ny(ny_)
    {}
    virtual void process(Box2D domain, BlockLattice2D<T,Descriptor>& lattice) {
        Dot2D absoluteOffset = lattice.getLocation();
        for (plint iX=domain.x0; iX<=domain.x1; ++iX) {
            plint absX = iX + absoluteOffset.x;
            for (plint iY=domain.y0; iY<=domain.y1; ++iY) {
                plint absY = iY + absoluteOffset.y;
                plint mask = std::round( lattice.get(iX,iY).computeDensity() );
                if (mask == 4) {
                    std::vector<T> locXY0; plint len = 0;
                    if ( absX == 0 && absY != 0 && absY != ny-1 ) {
                        locXY0.push_back(iX+1); locXY0.push_back(iY);
                        locXY0.push_back(iX); locXY0.push_back(iY+1);
                        locXY0.push_back(iX); locXY0.push_back(iY-1);
                        len = 3;
                    }
                    else if ( absX == (nx-1) && absY != 0 && absY != ny-1 ) {
                        locXY0.push_back(iX-1); locXY0.push_back(iY);
                        locXY0.push_back(iX); locXY0.push_back(iY+1);
                        locXY0.push_back(iX); locXY0.push_back(iY-1);
                        len = 3;
                    }
                    else if ( absX != 0 && absX != (nx-1) && absY == 0 ) {
                        locXY0.push_back(iX-1); locXY0.push_back(iY);
                        locXY0.push_back(iX+1); locXY0.push_back(iY);
                        locXY0.push_back(iX); locXY0.push_back(iY+1);
                        len = 3;
                    }
                    else if ( absX != 0 && absX != (nx-1) && absY == ny-1 ) {
                        locXY0.push_back(iX-1); locXY0.push_back(iY);
                        locXY0.push_back(iX+1); locXY0.push_back(iY);
                        locXY0.push_back(iX); locXY0.push_back(iY-1);
                        len = 3;
                    }
                    else if ( absX == 0 && absY == 0 ) {
                        locXY0.push_back(iX+1); locXY0.push_back(iY);
                        locXY0.push_back(iX); locXY0.push_back(iY+1);
                        len = 2;
                    }
                    else if ( absX == 0 && absY == ny-1 ) {
                        locXY0.push_back(iX+1); locXY0.push_back(iY);
                        locXY0.push_back(iX); locXY0.push_back(iY-1);
                        len = 2;
                    }
                    else if ( absX == (nx-1) && absY == 0 ) {
                        locXY0.push_back(iX-1); locXY0.push_back(iY);
                        locXY0.push_back(iX); locXY0.push_back(iY+1);
                        len = 2;
                    }
                    else if ( absX == (nx-1) && absY == ny-1 ) {
                        locXY0.push_back(iX-1); locXY0.push_back(iY);
                        locXY0.push_back(iX); locXY0.push_back(iY-1);
                        len = 2;
                    }
                    else {
                        locXY0.push_back(iX+1); locXY0.push_back(iY);
                        locXY0.push_back(iX-1); locXY0.push_back(iY);
                        locXY0.push_back(iX); locXY0.push_back(iY+1);
                        locXY0.push_back(iX); locXY0.push_back(iY-1);
                        len = 4;
                    }
                    plint numberCount=0;
                    for (plint iT=0; iT<len; ++iT) {
                        plint tmpMask = std::round( lattice.get(locXY0[iT*2],locXY0[iT*2+1]).computeDensity() );
                        if ( mask == 4 && ((tmpMask > 0 && tmpMask < 5) || tmpMask > 6) ) {
                            ++numberCount;
                        }
                    }
                    if (mask == 4 && numberCount == len) {
                        Array<T,5> g;
                        g[0]=(T) (2-1)/3; g[1]=g[2]=g[3]=g[4]=(T) (2-1)/6;
                        lattice.get(iX,iY).setPopulations(g); // allocate the mask number
                    }
                }
            }
        }
    }
    virtual BlockDomain::DomainT appliesTo() const {
        return BlockDomain::bulk;
    }
    virtual nonlocalMaskNumberUpdate2D<T,Descriptor>* clone() const {
        return new nonlocalMaskNumberUpdate2D<T,Descriptor>(*this);
    }
    void getTypeOfModification(std::vector<modif::ModifT>& modified) const {
        modified[0] = modif::staticVariables;
    }
private:
    plint nx;
    plint ny;
};

// Update local mask number at every 10 timesteps for flow field update
template<typename T1, template<typename U> class Descriptor1, typename T2, template<typename U> class Descriptor2>
class UpdateInterfaceMaskNumbers2D : public BoxProcessingFunctional2D_LL<T1,Descriptor1,T2,Descriptor2>
{
public:
    UpdateInterfaceMaskNumbers2D(T thrs_) : halfThrs(thrs_*0.5)
    {}
    // lattice1 = surface-associated biomass 1
    // lattice2 = mask field

    virtual void process(Box2D domain, BlockLattice2D<T1,Descriptor1>& lattice1, BlockLattice2D<T2,Descriptor2>& lattice2) {
        Dot2D offset_12 = computeRelativeDisplacement(lattice1,lattice2);
        for (plint iX1=domain.x0; iX1<=domain.x1; ++iX1) {
            plint iX2 = iX1 + offset_12.x;
            for (plint iY1=domain.y0; iY1<=domain.y1; ++iY1) {
                plint iY2 = iY1 + offset_12.y;
                plint mask = std::round(lattice2.get(iX2,iY2).computeDensity());
                if (mask == 3 || mask == 7) {
                    Array<T,5> g;
                    T B2 = lattice1.get(iX1,iY1).computeDensity();
                    if (mask == 3 && B2 >= halfThrs) { // expanding sessile biomass
                        g[0]=(T) 1; g[1]=g[2]=g[3]=g[4]=(T) 0.5; // convert mask 3 to mask 4
                        lattice2.get(iX2,iY2).setPopulations(g);
                    }
                    else if (mask == 7 && B2 >= halfThrs) { // shirinking sessile biomass
                        g[0]=(T) (8-1)/3; g[1]=g[2]=g[3]=g[4]=(T) (8-1)/6; // convert mask 7 to mask 8
                        lattice2.get(iX2,iY2).setPopulations(g);
                    }
                    else if (B2 < 0) { // retreating (need to code up if necessary)
                        std::cout << "ERROR: sessile biomass goes negative. Terminating the simulation (mask = " << mask << ").\n";
                        exit(EXIT_FAILURE);
                    }
                }
            }
        }
    }
    virtual BlockDomain::DomainT appliesTo() const {
        return BlockDomain::bulkAndEnvelope;
    }
    virtual UpdateInterfaceMaskNumbers2D<T1,Descriptor1,T2,Descriptor2>* clone() const {
        return new UpdateInterfaceMaskNumbers2D<T1,Descriptor1,T2,Descriptor2>(*this);
    }
    void getTypeOfModification(std::vector<modif::ModifT>& modified) const {
        modified[0] = modif::nothing;
        modified[1] = modif::staticVariables;
    }
private:
    T halfThrs;
};

// Link geometry scalar numbers and maskLattice
template<typename T1, template<typename U> class Descriptor, typename T2>
class LinkGeometryScalarNmaskLattice2D : public BoxProcessingFunctional2D_LS<T1,Descriptor,T2>
{
public:
    LinkGeometryScalarNmaskLattice2D(bool biID_) : biID(biID_)
    {}
    virtual void process(Box2D domain, BlockLattice2D<T1,Descriptor>& lattice, ScalarField2D<T2> &field) {
        Dot2D offset = computeRelativeDisplacement(lattice, field);
        for (plint iX=domain.x0; iX<=domain.x1; ++iX) {
            for (plint iY=domain.y0; iY<=domain.y1; ++iY) {
                if (biID == 0) { // lattice to scalar
                    plint mask = std::round( lattice.get(iX,iY).computeDensity() );
                    field.get(iX+offset.x,iY+offset.y) = mask;
                }
                else if (biID == 1) { // scalar to lattice
                    Array<T,5> g;
                    T mask = field.get(iX,iY);
                    if (mask == 0) {
                        g[0]=(T) (5-1)/3; g[1]=g[2]=g[3]=g[4]=(T) (5-1)/6; // assign 5 for inner solid nodes (no dynamics)
                    }
                    else if (mask == 1) {
                        g[0]=g[1]=g[2]=g[3]=g[4]=(T) 0.; // assign 1 for bounce-back boundary
                    }
                    else if (mask == 2) {
                        g[0]=(T) (7-1)/3; g[1]=g[2]=g[3]=g[4]=(T) (7-1)/6; // assign 7 for initial biomass nodes
                    }
                    else {
                        g[0]=(T) -1/3; g[1]=g[2]=g[3]=g[4]=(T) -1/6; // assign 0 for fluid nodes
                    }
                    lattice.get(iX,iY).setPopulations(g); // allocate the mask number
                }
            }
        }
    }
    virtual LinkGeometryScalarNmaskLattice2D<T1,Descriptor,T2>* clone() const {
        return new LinkGeometryScalarNmaskLattice2D<T1,Descriptor,T2>(*this);
    }
    virtual BlockDomain::DomainT appliesTo() const {
        return BlockDomain::bulkAndEnvelope;
    }
    void getTypeOfModification(std::vector<modif::ModifT>& modified) const {
        modified[0] = modif::staticVariables;
        modified[1] = modif::staticVariables;
    }
private:
    bool biID;
};

// Link geometry scalar numbers and maskLattice
template<typename T1, template<typename U> class Descriptor, typename T2>
class CopyGeometryScalarNmaskLattice2D : public BoxProcessingFunctional2D_LS<T1,Descriptor,T2>
{
public:
    CopyGeometryScalarNmaskLattice2D(bool biID_) : biID(biID_)
    {}
    virtual void process(Box2D domain, BlockLattice2D<T1,Descriptor>& lattice, ScalarField2D<T2> &field) {
        Dot2D offset = computeRelativeDisplacement(lattice, field);
        for (plint iX=domain.x0; iX<=domain.x1; ++iX) {
            for (plint iY=domain.y0; iY<=domain.y1; ++iY) {
                if (biID == 0) { // lattice to scalar
                    plint mask = std::round( lattice.get(iX,iY).computeDensity() );
                    field.get(iX+offset.x,iY+offset.y) = mask;
                }
                else if (biID == 1) { // scalar to lattice
                    Array<T,5> g;
                    T mask = field.get(iX,iY);
                    g[0]=(T) (mask-1)/3; g[1]=g[2]=g[3]=g[4]=(T) (mask-1)/6;
                    lattice.get(iX,iY).setPopulations(g); // allocate the mask number
                }
            }
        }
    }
    virtual CopyGeometryScalarNmaskLattice2D<T1,Descriptor,T2>* clone() const {
        return new CopyGeometryScalarNmaskLattice2D<T1,Descriptor,T2>(*this);
    }
    virtual BlockDomain::DomainT appliesTo() const {
        return BlockDomain::bulkAndEnvelope;
    }
    void getTypeOfModification(std::vector<modif::ModifT>& modified) const {
        modified[0] = modif::staticVariables;
        modified[1] = modif::staticVariables;
    }
private:
    bool biID;
};

// update the flow field
template<typename T1, template<typename U> class Descriptor1, typename T2, template<typename U> class Descriptor2>
class UpdateNsLatticesDynamics2D : public BoxProcessingFunctional2D_LL<T1,Descriptor1,T2,Descriptor2>
{
public:
    UpdateNsLatticesDynamics2D(T nsOmega_) : nsOmega(nsOmega_)
    {}
    // lattice1 = flow field lattice
    // lattice2 = mask field lattice
    virtual void process(Box2D domain, BlockLattice2D<T1,Descriptor1>& lattice1, BlockLattice2D<T2,Descriptor2>& lattice2) {
        Dot2D offset_12 = computeRelativeDisplacement(lattice1,lattice2);
        for (plint iX=domain.x0; iX<=domain.x1; ++iX) {
            for (plint iY=domain.y0; iY<=domain.y1; ++iY) {
                T fluidOmega = lattice1.get(iX,iY).getDynamics().getOmega();
                plint mask = std::round( lattice2.get(iX+offset_12.x,iY+offset_12.y).computeDensity() );
                if ( (mask == 2 || mask == 4 || mask == 8) && fluidOmega == nsOmega ) {
                	T bfomega = 1/(3*(1/nsOmega-.5)+.5);
                    lattice1.attributeDynamics(iX, iY, new IncBGKdynamics<T1,Descriptor1>(bfomega) );
                }
                else if ( (mask == 0 || mask == 3 || mask == 7) && fluidOmega != nsOmega ) {
                    lattice1.attributeDynamics(iX, iY, new IncBGKdynamics<T1,Descriptor1>(nsOmega) );
                }
            }
        }
    }
    virtual UpdateNsLatticesDynamics2D<T1,Descriptor1,T2,Descriptor2>* clone() const {
        return new UpdateNsLatticesDynamics2D<T1,Descriptor1,T2,Descriptor2>(*this);
    }
    virtual BlockDomain::DomainT appliesTo() const {
        return BlockDomain::bulkAndEnvelope;
    }
    void getTypeOfModification(std::vector<modif::ModifT>& modified) const {
        modified[0] = modif::dataStructure;
        modified[1] = modif::nothing;
    }
private :
    T nsOmega;
};

// lattice divisions for MPI
template<typename T1, template<typename U> class Descriptor, typename T2>
class latticeXY2D : public BoxProcessingFunctional2D_LS<T1,Descriptor,T2>
{
public:
    latticeXY2D(bool biId_): biId(biId_)
    {}
    virtual void process(Box2D domain, BlockLattice2D<T1,Descriptor>& lattice, ScalarField2D<T2> &field) {
        Dot2D offset = computeRelativeDisplacement(lattice, field);
        for (plint iX=domain.x0; iX<=domain.x1; ++iX) {
            plint iX1 = iX + offset.x;
            for (plint iY=domain.y0; iY<=domain.y1; ++iY) {
                plint iY1 = iY + offset.y;
                if (biId == 0) {
                    field.get(iX1,iY1) = iX;
                }
                else {
                    field.get(iX1,iY1) = iY;
                }
            }
        }
    }
    virtual latticeXY2D<T1,Descriptor,T2>* clone() const {
        return new latticeXY2D<T1,Descriptor,T2>(*this);
    }
    virtual BlockDomain::DomainT appliesTo() const {
        return BlockDomain::bulkAndEnvelope;
    }
    void getTypeOfModification(std::vector<modif::ModifT>& modified) const {
        modified[0] = modif::nothing;
        modified[1] = modif::staticVariables;
    }
private:
    bool biId;
};

// initialize scalar biomass lattice
template<typename T1, template<typename U> class Descriptor, typename T2>
class initializeMaskedScalarLattice : public BoxProcessingFunctional2D_LS<T1,Descriptor,T2>
{
public:
    initializeMaskedScalarLattice(T rho0_, plint id_): rho0(rho0_), id(id_)
    {}
    virtual void process(Box2D domain, BlockLattice2D<T1,Descriptor>& lattice, ScalarField2D<T2> &field) {
        Dot2D offset = computeRelativeDisplacement(lattice, field);
        for (plint iX=domain.x0; iX<=domain.x1; ++iX) {
            plint iX1 = iX + offset.x;
            for (plint iY=domain.y0; iY<=domain.y1; ++iY) {
                plint iY1 = iY + offset.y;
                T2 mask = field.get(iX1,iY1);
                if (mask == id) {
                    Array<T,5> g;
                    g[0]=(T) (rho0-1)/3; g[1]=g[2]=g[3]=g[4]=(T) (rho0-1)/6;
                    lattice.get(iX,iY).setPopulations(g);
                }
            }
        }
    }
    virtual initializeMaskedScalarLattice<T1,Descriptor,T2>* clone() const {
        return new initializeMaskedScalarLattice<T1,Descriptor,T2>(*this);
    }
    virtual BlockDomain::DomainT appliesTo() const {
        return BlockDomain::bulkAndEnvelope;
    }
    void getTypeOfModification(std::vector<modif::ModifT>& modified) const {
        modified[0] = modif::staticVariables;
        modified[1] = modif::nothing;
    }
private:
    T rho0;
    plint id;
};

/* =============================================================================================================== 
   ========================================== REDUCTIVE DATA PROCESSORS ========================================== 
   =============================================================================================================== */

// count the number of a certain mask
template<typename T1, template<typename U1> class Descriptor1>
class BoxScalarSelectedSumFunctional2D : public ReductiveBoxProcessingFunctional2D_L<T1,Descriptor1>
{
public:
    BoxScalarSelectedSumFunctional2D(plint mask1_, plint mask2_, plint mask3_) : countId(this->getStatistics().subscribeSum()), mask1(mask1_), mask2(mask2_), mask3(mask3_)
    {}
    virtual void process(Box2D domain, BlockLattice2D<T1,Descriptor1>& lattice1) {
        BlockStatistics& statistics = this->getStatistics();
        for (plint iX=domain.x0; iX<=domain.x1; ++iX) {
            for (plint iY=domain.y0; iY<=domain.y1; ++iY) {
                plint tmpMask = util::roundToInt(lattice1.get(iX,iY).computeDensity());
                if ( tmpMask == mask1 || tmpMask == mask2 || tmpMask == mask3) {
                    statistics.gatherSum(countId, (int) 1);
                }
            }
        }
    }
    virtual BoxScalarSelectedSumFunctional2D<T1,Descriptor1>* clone() const {
        return new BoxScalarSelectedSumFunctional2D<T1,Descriptor1>(*this);
    }
    virtual void getTypeOfModification(std::vector<modif::ModifT>& modified) const {
        modified[0] = modif::nothing;
    }
    T getCount() const {
        // The sum is internally computed on floating-point values. If T is
        //   integer, the value must be rounded at the end.
        double doubleSum = this->getStatistics().getSum(countId);
        if (std::numeric_limits<T>::is_integer) {
            return (T) util::roundToInt(doubleSum);
        }
        return (T) doubleSum;
    }
private:
    plint countId;
    plint mask1;
    plint mask2;
    plint mask3;
};

template<typename T1, template<typename U1> class Descriptor1>
T countMaskNumbers(Box2D domain, MultiBlockLattice2D<T1,Descriptor1>& lattice1, plint maskNumb1, plint maskNumb2, plint maskNumb3) {
    BoxScalarSelectedSumFunctional2D<T1,Descriptor1> functional = BoxScalarSelectedSumFunctional2D<T1,Descriptor1>(maskNumb1,maskNumb2,maskNumb3);
    applyProcessingFunctional(functional, domain, lattice1);
    return functional.getCount();
}

// calculate the maximum density with a mask lattice
template<typename T1, template<typename U1> class Descriptor1, typename T2, template<typename U2> class Descriptor2>
class MaskedBoxLatticeMaxFunctional2D : public ReductiveBoxProcessingFunctional2D_LL<T1,Descriptor1,T2,Descriptor2>
{
public:
    MaskedBoxLatticeMaxFunctional2D() : maxLatticeId(this->getStatistics().subscribeMax())
    {}
    virtual void process(Box2D domain, BlockLattice2D<T1,Descriptor1> &lattice0, BlockLattice2D<T2,Descriptor2> &lattice1) {
        BlockStatistics& statistics = this->getStatistics();
        Dot2D offset_01 = computeRelativeDisplacement(lattice0,lattice1);
        for (plint iX0=domain.x0; iX0<=domain.x1; ++iX0) {
            for (plint iY0=domain.y0; iY0<=domain.y1; ++iY0) {
                plint mask = std::round( lattice1.get(iX0+offset_01.x,iY0+offset_01.y).computeDensity() );
                if (mask == 2 || mask == 4 || mask == 7 || mask == 8) {
                    T max = lattice0.get(iX0,iY0).computeDensity();
                    statistics.gatherMax(maxLatticeId, max);
                }
            }
        }
    }
    virtual MaskedBoxLatticeMaxFunctional2D<T1,Descriptor1,T2,Descriptor2>* clone() const {
        return new MaskedBoxLatticeMaxFunctional2D<T1,Descriptor1,T2,Descriptor2>(*this);
    }
    virtual void getTypeOfModification(std::vector<modif::ModifT>& modified) const {
        modified[0] = modif::nothing;
        modified[1] = modif::nothing;
    }
    T getMaxLattice() const {
        // The sum is internally computed on floating-point values. If T is
        //   integer, the value must be rounded at the end.
        double doubleMax = this->getStatistics().getMax(maxLatticeId);
        if (std::numeric_limits<T>::is_integer) {
            return (T) util::roundToInt(doubleMax);
        }
        return (T) doubleMax;
    }
private:
    plint maxLatticeId;
};

template<typename T1, template<typename U1> class Descriptor1, typename T2, template<typename U2> class Descriptor2>
T computeMaskedLatticeMax(Box2D domain, MultiBlockLattice2D<T1,Descriptor1>& lattice0, MultiBlockLattice2D<T2,Descriptor2>& lattice1) {
    MaskedBoxLatticeMaxFunctional2D<T1,Descriptor1,T2,Descriptor2> functional;
    applyProcessingFunctional(functional, domain, lattice0, lattice1);
    return functional.getMaxLattice();
}

// =============================================== MAIN PROGRAM =============================================== //

int main(int argc, char **argv)
{
    plbInit(&argc, &argv);

    if (argc!=11) {
        pcout << "Error missing some input parameter\n";
        pcout << "The structure is :\n";
        pcout << "1. Input geometry 1 file name.\n";
        pcout << "2. Input geometry 2 file name.\n";
        pcout << "3. nx.\n";
        pcout << "4. ny.\n";
        pcout << "5. dx.\n";
        pcout << "6. deltaP.\n";
        pcout << "7. NS tau.\n";
        pcout << "8. Peclet number.\n";
        pcout << "9. Diffusive Da_A.\n";
        pcout << "10. Continuous Simulation (use the field but no = 2,  yes = 1, no = 0).\n";
        pcout << "Example: " << argv[0] << " geometry.dat 1001 501 10 0.001 1 1 2 1\n";
        exit (EXIT_FAILURE);
    }
    std::string fNameIn  = argv[1];
    std::string fNameIn2  = argv[2];
    const plint nx = atof(argv[3]);
    const plint ny = atof(argv[4]);
    const T dx_lu = atof(argv[5]);
    const T dx = dx_lu*1e-6;
    const T deltaP = atof(argv[6]);
    const T nsLatticeTau = atof(argv[7]);
    const T Pe = atof(argv[8]);
    const T DaII_A = atof(argv[9]);
    const plint contSim = atof(argv[10]);

    const T DaI_A = DaII_A / Pe;

    struct stat statStruct;
    const char *dirname = "output/";
    
    time_t tstart, tend;
    srand ( unsigned ( std::time(0) ) );
 
    stat(dirname, &statStruct);
 
    if (S_ISDIR(statStruct.st_mode)) {
        pcout << "output folder already exists" << std::endl;
    }
    else {
        pcout << "Creating output folder (./output)" << std::endl;
        mkdir(dirname, 0777);
    }
    global::directories().setOutputDir(dirname);

    MultiScalarField2D<int> geometry(nx,ny);
    MultiScalarField2D<int> geometry2(nx,ny);
    MultiScalarField2D<int> flagMatrix(nx,ny);
    MultiScalarField2D<int> iXindex(nx,ny);
    MultiScalarField2D<int> iYindex(nx,ny);
    readGeometry(fNameIn, geometry);
    readGeometry(fNameIn2, geometry2);
    flagMatrix = geometry;
    for (plint iX=0; iX<nx; ++iX) {
        for (plint iY=0; iY<ny; ++iY) {
            if (flagMatrix.get(iX,iY)<=1) {
                std::auto_ptr<MultiScalarField2D<int> > slice = generateMultiScalarField<int>(Box2D(0,0,0,0), (int) 1, 1);
                copy(*slice, Box2D(0,0,0,0), flagMatrix, Box2D(iX,iX,iY,iY));
            }
            else {
                std::auto_ptr<MultiScalarField2D<int> > slice = generateMultiScalarField<int>(Box2D(0,0,0,0), (int) 0, 1);
                copy(*slice, Box2D(0,0,0,0), flagMatrix, Box2D(iX,iX,iY,iY));
            }
        }
    }
    pcout << std::endl;
    writePorousMediumVTK(flagMatrix, 1);
/* ======================================================================================= 
// =================================== nsLattice setup ===================================
   ======================================================================================= */

    const T latticeLenC = 100; // characteristic length
    const T nsLatticeOmega = 1 / nsLatticeTau;
    const T nsLatticeNu = NSDES<T>::cs2*(nsLatticeTau-0.5);
    pcout << "nsLatticeTau = " << nsLatticeTau <<", nsLatticeOmega = " << nsLatticeOmega << ", nsLatticeNu = " << nsLatticeNu << std::endl << std::endl;

    pcout << "Instantiate the fluid lattice." << std::endl;
    MultiBlockLattice2D<T,NSDES> nsLattice1(nx, ny, new IncBGKdynamics<T,NSDES>(nsLatticeOmega));

    NSdomainSetup(nsLattice1, createLocalBoundaryCondition2D<T,NSDES>(), geometry, deltaP, nsLatticeOmega);

/*  =================================== NS Main Loop ===================================  */
    int iT = 0; bool nsSim=0;
    const plint nsiTmax  = 10000000;
    const char *NS1name = "./output/nsIntLattice1CheckPoint.dat";

    pcout << "LBM NS simulation begins \n \n";
    ImageWriter<T> image("leeloo");
    util::ValueTracer<T> converge(1.0,1000.0,1.e-8);

    T PoreMeanU, PoreMaxUx, DarcyOutletUx, DarcyMiddleUx, DarcyInletUx;
    if (Pe > 0) {
        if (fexists(NS1name) == 1) {
            pcout << "checkpoint file exists for nsLattice1 \n";
            if (contSim == 1) {
                pcout << "run continuous simulation for nsLattice1" << std::endl;
                pcout << "load binary block for nsLattice1" << std::endl;
                loadBinaryBlock(nsLattice1, NS1name);
                pcout << "run main nsLattice1 loop for nsLattice1" << std::endl;
                for (; iT < nsiTmax; ++iT) {
                    nsLattice1.collideAndStream();
                    converge.takeValue(getStoredAverageEnergy(nsLattice1),true);
                    if (converge.hasConverged()) {
                        break;
                    }
                }
                nsSim = 1;
            }
            else if (contSim == 2) {
                pcout << "use the existing checkpoint file for nsLattice1" << std::endl;
                pcout << "load binary block for nsLattice1" << std::endl;
                loadBinaryBlock(nsLattice1, NS1name);
                nsSim = 1;
            }
            else if (contSim == 0) {
                pcout << "Run a new simulation for nsLattice1" << std::endl;
                for (; iT < nsiTmax; ++iT) {
                    nsLattice1.collideAndStream();
                    converge.takeValue(getStoredAverageEnergy(nsLattice1),true);
                    if (converge.hasConverged()) {
                        break;
                    }
                }
                nsSim = 1;
            }
        }
        else {
            pcout << "checkpoint file doesn't exist for nsLattice1" << std::endl;
            pcout << "run main nsLattice1 loop for nsLattice1" << std::endl;
            for (; iT < nsiTmax; ++iT) {
                nsLattice1.collideAndStream();
                converge.takeValue(getStoredAverageEnergy(nsLattice1),true);
                if (converge.hasConverged()) {
                    break;
                }
            }
            nsSim = 1;
        }

        if (nsSim == 1) {
            pcout << std::endl << "flow calc finished at iT = " << iT << std::endl;
            pcout << "Writing velocity VTK... \n";
            writeNsVTK(nsLattice1,nsiTmax,"ns1LatticeFinal");
            pcout << "Writing checkpoint... \n\n";
            saveBinaryBlock(nsLattice1, NS1name);
        }

        PoreMeanU = computeAverage(*computeVelocityNorm(nsLattice1, Box2D (1,nx-2,0,ny-1)), flagMatrix, 0);
        PoreMaxUx = computeMax(*computeVelocityComponent(nsLattice1, Box2D (1,nx-2,0,ny-1), 0));
        DarcyOutletUx = computeAverage(*computeVelocityComponent(nsLattice1, Box2D (nx-2,nx-2, 0,ny-1), 0));
        DarcyMiddleUx = computeAverage(*computeVelocityComponent(nsLattice1, Box2D ((nx-1)/2,(nx-1)/2, 0,ny-1), 0));
        DarcyInletUx = computeAverage(*computeVelocityComponent(nsLattice1, Box2D (1,1, 0,ny-1), 0));

        pcout << "Outlet Darcy Ux = " << DarcyOutletUx << " lu/ts" << std::endl;
        pcout << "Middle Darcy Ux = " << DarcyMiddleUx << " lu/ts" << std::endl;
        pcout << "Inlet Darcy Ux = " << DarcyInletUx << " lu/ts" << std::endl;
        pcout << "CFL number1 (= maximum local lattice velocity)= " << PoreMaxUx << std::endl;
        pcout << "Mach number1 = " << PoreMaxUx/sqrt(RXNDES<T>::cs2) << std::endl  << std::endl;

    }

/* ================================================================================================================================================================================
   =========================================================================== rxnLattice setup ===================================================================================
   ================================================================================================================================================================================ */

    const T physD = 1e-9;
    const T withinBMassD = physD*0.8;

    T rxnLatticeNu;
    if (Pe > 0) {
        rxnLatticeNu = PoreMeanU * latticeLenC / Pe;
    }
    else {
        rxnLatticeNu = RXNDES<T>::cs2*0.5;
    }
    const T rxnLatticeTau = rxnLatticeNu * RXNDES<T>::invCs2 + 0.5;
    const T rxnLatticeOmega = 1/rxnLatticeTau;

    const T rxnLatticebMassNu = rxnLatticeNu * withinBMassD / physD;
    const T rxnLatticebMassTau = rxnLatticebMassNu * RXNDES<T>::invCs2 + 0.5;
    const T rxnLatticebMassOmega = 1/rxnLatticebMassTau;

    T ade_dt = RXNDES<T>::cs2 * (rxnLatticeTau - 0.5) * dx * dx / physD;

    T Yield  = 0.1;
    T B0  = 0.1;
    T Km = 0.16; // nondimensionalized
    T Bmax = 1; // Bmax should be adjusted for simulations with porous biofilm. e.g. if biofilm porosity = 0.3, Bmax = 0.7
    T kc = DaII_A * rxnLatticeNu / (latticeLenC * latticeLenC); // (kc = kc' * Bmax'/c0'), nondimensionalized

    params[0] = kc;
    params[1] = Km;
    params[2] = Yield;

    pcout << "rxnLatticeTau = " << rxnLatticeTau << ", rxnLatticebMassTau = " << rxnLatticebMassTau << std::endl;
    if (Pe > 0) {
        pcout << "Peclet Number (outlet) = " << DarcyOutletUx * latticeLenC / rxnLatticeNu << ", Peclet Number (meanU) = " << PoreMeanU * latticeLenC / rxnLatticeNu << ", Grid Peclet Number (maxU) = " << PoreMaxUx / rxnLatticeNu << std::endl;
    }
    pcout << "Advective Damkohler number A = " << DaI_A << ", Diffusive Damkohler number A = " << DaII_A << std::endl;
    pcout << "kc = " << kc << ", Km = " << Km << ", ade_dt = " << ade_dt << std::endl;

    pcout << "Instantiate the reaction lattices." << std::endl;
    MultiBlockLattice2D<T,RXNDES> rxn1Lattice(nx, ny, new AdvectionDiffusionBGKdynamics<T,RXNDES>(rxnLatticeOmega));
    MultiBlockLattice2D<T,RXNDES> rxnTmpLattice(nx, ny, new AdvectionDiffusionBGKdynamics<T,RXNDES>(rxnLatticeOmega));
    MultiBlockLattice2D<T,RXNDES> mask1Lattice(nx, ny, new AdvectionDiffusionBGKdynamics<T,RXNDES>(1.)); // mask lattice for phase differentiation
    MultiBlockLattice2D<T,RXNDES> mask2Lattice(nx, ny, new AdvectionDiffusionBGKdynamics<T,RXNDES>(1.)); // mask lattice for excess biomass push
    MultiBlockLattice2D<T,RXNDES> invLattice(nx, ny, new AdvectionDiffusionBGKdynamics<T,RXNDES>(1.)); // mask lattice with the inversed numbers 
    MultiBlockLattice2D<T,RXNDES> bMass1SfcLattice(nx, ny, new AdvectionDiffusionBGKdynamics<T,RXNDES>(1.));
    MultiBlockLattice2D<T,RXNDES> bMass1SfcTmpLattice(nx, ny, new AdvectionDiffusionBGKdynamics<T,RXNDES>(1.));
    MultiBlockLattice2D<T,RXNDES> storageLattice(nx, ny, new AdvectionDiffusionBGKdynamics<T,RXNDES>(1.));
    MultiBlockLattice2D<T,RXNDES> storageLatticeInit(nx, ny, new AdvectionDiffusionBGKdynamics<T,RXNDES>(1.));
    MultiBlockLattice2D<T,RXNDES> bdryDirectionLattice(nx, ny, new AdvectionDiffusionBGKdynamics<T,RXNDES>(1.));
    MultiBlockLattice2D<T,RXNDES> bdryDirectionLatticeInit(nx, ny, new AdvectionDiffusionBGKdynamics<T,RXNDES>(1.));

    applyProcessingFunctional(new LinkGeometryScalarNmaskLattice2D<T,RXNDES,int> (1), mask1Lattice.getBoundingBox(), mask1Lattice, geometry);
    applyProcessingFunctional(new CopyGeometryScalarNmaskLattice2D<T,RXNDES,int> (1), mask2Lattice.getBoundingBox(), mask2Lattice, geometry);
    applyProcessingFunctional(new CopyGeometryScalarNmaskLattice2D<T,RXNDES,int> (1), invLattice.getBoundingBox(), invLattice, geometry2);
    const T poreLen = countMaskNumbers(mask1Lattice.getBoundingBox(),mask1Lattice,0,3,6);

    solute1DomainSetup(rxn1Lattice, createLocalAdvectionDiffusionBoundaryCondition2D<T,RXNDES>(), geometry, 1., 1., rxnLatticebMassOmega, rxnLatticeOmega); //geometry, initial conc., boundary conc., bMassOmega
    scalarDomainSetup (bMass1SfcLattice, geometry, 0., rxnLatticebMassOmega); //geometry, initial number of biomass
    scalarDomainSetup (storageLattice, geometry, -66., 1.); //geometry, initial number of biomass
    scalarDomainSetup (bdryDirectionLattice, geometry, -66., 1.); //geometry, initial number of biomass
    applyProcessingFunctional(new initializeMaskedScalarLattice<T,RXNDES,int> (B0, 2), bMass1SfcLattice.getBoundingBox(), bMass1SfcLattice, geometry); // attribute the initial biomass

    // First attempt to couple the two physics
    if (Pe > 0) {
        latticeToPassiveAdvDiff(nsLattice1, rxn1Lattice, rxn1Lattice.getBoundingBox());
    }

    rxnTmpLattice = rxn1Lattice;
    bMass1SfcTmpLattice = bMass1SfcLattice;
    storageLatticeInit = storageLattice;
    bdryDirectionLatticeInit = bdryDirectionLattice;

    pcout << "The initial mask number count (0, 3, and 6) = " << poreLen << std::endl;
    T poreLen0 = poreLen;

    // multi lattice vectors
    std::vector<MultiBlockLattice2D<T, RXNDES>* > localRXNLattices;
    localRXNLattices.push_back(&rxn1Lattice);
    localRXNLattices.push_back(&bMass1SfcLattice);
    localRXNLattices.push_back(&mask1Lattice);

    std::vector<MultiBlockLattice2D<T, RXNDES>* > nonlocalLattices;
    nonlocalLattices.push_back(&bMass1SfcLattice);
    nonlocalLattices.push_back(&mask1Lattice);
    nonlocalLattices.push_back(&storageLattice);
    nonlocalLattices.push_back(&bdryDirectionLattice);
    nonlocalLattices.push_back(&mask2Lattice);
    nonlocalLattices.push_back(&invLattice);

/*  ===============================================          ADV Main Loop         ====================================================  */

    iT = 0;
    const plint adviTmax = 3500000;
    plint iTNum1 = 50000;     // save VTK
    plint iTNum2 = 10000000;   // save checkpoints
    plint iTNum3 = 10;        // iterate velocity field
    T convThrs = 1.e-7;        // convergence threshold
    plint bdryGap = 2;

    pcout << "LBM ADRE simulation begins" << std::endl << std::endl;
    tstart=time(0);
    util::ValueTracer<T> converge2(1.0,1000.0,1.e-5);

    bool checker = 0;
    for (; iT < adviTmax; ++iT) {

        if ( (iT == 0) || (iT % iTNum1 == 0 && checker == 1) ) {
            T cur_time = iT*ade_dt;
            pcout << "Iteration = " << iT << "; cur_time = " << cur_time << " seconds" << std::endl;
            pcout << "Writing concentration VTK... \n";

            writeAdvVTK(rxn1Lattice, iT, "rxn1Lattice");
            // writeAdvVTK(mask1Lattice, iT, "mask1Lattice");
            writeAdvVTK(bMass1SfcLattice, iT, "bMass1SurfaceLattice");
            if (Pe > 0) {
                writeNsVTK(nsLattice1, iT, "ns1Lattice");
            }

            tend=time(0);
            pcout << "Time elapsed: " << difftime(tend, tstart) << "seconds." << std::endl;
            tstart=time(0);
        }

        if (iT % iTNum2 == 0 && iT > 0) {
            pcout << "Writing checkpoint... \n";
            saveBinaryBlock(rxn1Lattice, "./output/rxn1LatticeCheckPoint" + std::to_string(iT) + ".dat");
            saveBinaryBlock(bMass1SfcLattice, "./output/bMass1SfcCheckpoint" + std::to_string(iT) + ".dat");
            saveBinaryBlock(mask1Lattice, "./output/mask1LatticeCheckPoint" + std::to_string(iT) + ".dat");
            if (Pe > 0) {
                saveBinaryBlock(nsLattice1, "./output/nsLattice1CheckPoint" + std::to_string(iT) + ".dat");
            }
        }

        rxn1Lattice.collide();

        // local biomass growth / death / detachment
        applyProcessingFunctional(new localReactionLattices2D<T,RXNDES> (1.), rxn1Lattice.getBoundingBox(), localRXNLattices);

        // redistribute the exccess biomasss
        T globalB2max = computeMaskedLatticeMax(bMass1SfcLattice.getBoundingBox(),bMass1SfcLattice,mask1Lattice);
        if (std::isnan(globalB2max) == 1) {
            pcout << "ERROR: B2 goes wrong.\n";
            exit(EXIT_FAILURE);
        }
        bool updated = 0;

        while (globalB2max > Bmax) {
            storageLattice = storageLatticeInit;
            bdryDirectionLattice = bdryDirectionLatticeInit;
            applyProcessingFunctional(new pushExcessBiomass2D<T,RXNDES> (Bmax, nx, ny, bdryGap), bMass1SfcLattice.getBoundingBox(), nonlocalLattices);
            applyProcessingFunctional(new pullInterfaceBiomass2D<T,RXNDES> (nx, ny, bdryGap), bMass1SfcLattice.getBoundingBox(), nonlocalLattices);
            globalB2max = computeMaskedLatticeMax(bMass1SfcLattice.getBoundingBox(),bMass1SfcLattice,mask1Lattice);
            updated = 1;
        }

        // update mask number 4 to 2 or 2 to 4
        if (updated == 1) {
            applyProcessingFunctional(new nonlocalMaskNumberUpdate2D<T,RXNDES> (nx, ny), mask1Lattice.getBoundingBox(), mask1Lattice);
        }

        // Update flow field every iTNum3 time steps
        if (iT % iTNum3 == 0 && iT > 0) {
            // update the expanding sessile biomass at interfaces (from 3 to 4 or 4 to 3)
            applyProcessingFunctional(new UpdateInterfaceMaskNumbers2D<T,RXNDES,T,RXNDES> (Bmax), bMass1SfcLattice.getBoundingBox(), bMass1SfcLattice, mask1Lattice);
            if (Pe > 0) {
                T poreLen1 = countMaskNumbers(mask1Lattice.getBoundingBox(),mask1Lattice,0,3,6);
                bool stopper = 0;
                if (poreLen0 != poreLen1) {
                    poreLen0 = poreLen1;
                    pcout << "geometry updated at iT = " << iT << " and dporeLen = " << poreLen0 << ". Recompute the flow field." << std::endl;
                    if (checker == 0) { checker = 1; }
                    // update dynamic variables of the fluid solver
                    applyProcessingFunctional(new UpdateNsLatticesDynamics2D<T,NSDES,T,RXNDES> (nsLatticeOmega), nsLattice1.getBoundingBox(), nsLattice1, mask1Lattice);
                    for (plint iTT = 0; iTT <iTNum1; ++iTT) {
                        nsLattice1.collideAndStream();
                        converge2.takeValue(getStoredAverageEnergy(nsLattice1),true);
                        if (converge2.hasConverged()) {
                            break;
                        }
                        if (converge2.computeAverage() < 1e-20) {
                            stopper = 1;
                            break;
                        }
                    }
                    if (stopper == 1) {
                        break;
                    }
                    latticeToPassiveAdvDiff(nsLattice1, rxn1Lattice, rxn1Lattice.getBoundingBox());
                }
            }
        }
        rxn1Lattice.stream();

    }

/* ===============================================          End of the ADE loop          =============================================== */

    pcout << "End of simulation at iteration " << iT << std::endl;

    pcout << "Writing the final VTK file ..." << std::endl << std::endl;
    writeAdvVTK(rxn1Lattice, iT, "rxn1Lattice");
    writeAdvVTK(bMass1SfcLattice, iT, "bMass1SurfaceLattice");
    writeAdvVTK(mask1Lattice,iT, "mask1Lattice");

    pcout << "Writing the final checkpoint... \n";
    saveBinaryBlock(rxn1Lattice, "./output/FinalDensityACheckpoint" + std::to_string(iT) + ".dat");
    saveBinaryBlock(bMass1SfcLattice, "./output/FinalbMass1SfcCheckpoint" + std::to_string(iT) + ".dat");
    saveBinaryBlock(mask1Lattice, "./output/FinalMask1Checkpoint" + std::to_string(iT) + ".dat");

    if (Pe > 0) {
        writeNsVTK(nsLattice1, iT, "ns1Lattice");
        saveBinaryBlock(nsLattice1, "./output/FinalnsLatticeCheckPoint" + std::to_string(iT) + ".dat");
    }    

    pcout << "Finished!" << std::endl << std::endl;

    return 0;

}
