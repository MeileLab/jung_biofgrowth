#include <string>
#include <iostream>
#include <vector>
#include <stdio.h>

#include <cvode/cvode.h>               /* prototypes for CVODE fcts., consts.  */
#include <nvector/nvector_serial.h>    /* access to serial N_Vector            */
#include <sunmatrix/sunmatrix_dense.h> /* access to dense SUNMatrix            */
#include <sunlinsol/sunlinsol_dense.h> /* access to dense SUNLinearSolver      */
#include <cvode/cvode_direct.h>        /* access to CVDls interface            */
#include <sundials/sundials_types.h>   /* defs. of realtype, sunindextype      */

#define Ith(v,i)    NV_Ith_S(v,i-1)         /* Ith numbers components 1..NEQ */
#define IJth(A,i,j) SM_ELEMENT_D(A,i-1,j-1) /* IJth numbers rows,cols 1..NEQ */
#define NEQ         2

// simulation parameters
static realtype params[3];

#define kac         params[0]
#define K0          params[1]
#define gamma       params[2]

int f1(realtype t, N_Vector y, N_Vector ydot, void *user_data)
{
  realtype c0, B2, rate;

  c0 = Ith(y,1); // solute 1 concentration
  B2 = Ith(y,2); // biomass

  rate = Ith(ydot,1) = -kac*B2*c0/(K0+c0);
  Ith(ydot,2) = -gamma*rate;

  return(0);
}

int Jac1(realtype t, N_Vector y, N_Vector fy, SUNMatrix J, 
               void *user_data, N_Vector tmp1, N_Vector tmp2, N_Vector tmp3)
{
  realtype c0, B2, rate1, rate2;

  c0 = Ith(y,1); // solute 1 concentration
  B2 = Ith(y,2); // biomass

  rate1 = IJth(J,1,1) = -kac*B2*K0/(c0+K0)/(c0+K0);
  rate2 = IJth(J,1,2) = -kac*c0/(c0+K0);

  IJth(J,2,1) = -gamma*rate1;
  IJth(J,2,2) = -gamma*rate2;

  return(0);
}


int check_flag(void *flagvalue, const char *funcname, int opt)
{
  int *errflag;

  /* Check if SUNDIALS function returned NULL pointer - no memory allocated */
  if (opt == 0 && flagvalue == NULL) {
    fprintf(stderr, "\nSUNDIALS_ERROR: %s() failed - returned NULL pointer\n\n",
        funcname);
    return(1); }

  /* Check if flag < 0 */
  else if (opt == 1) {
    errflag = (int *) flagvalue;
    if (*errflag < 0) {
      fprintf(stderr, "\nSUNDIALS_ERROR: %s() failed with flag = %d\n\n",
          funcname, *errflag);
      return(1); }}

  /* Check if function returned NULL pointer - no memory allocated */
  else if (opt == 2 && flagvalue == NULL) {
    fprintf(stderr, "\nMEMORY_ERROR: %s() failed - returned NULL pointer\n\n",
        funcname);
    return(1); }

  return(0);
}

int call_biomassGrowth(double &conc1, double &conc2, double dt)
{
  realtype reltol, t, tout;
  realtype RTOL, ATOL1, ATOL2;
  N_Vector y, abstol;
  SUNMatrix A;
  SUNLinearSolver LS;
  void *cvode_mem;
  int flag;

  realtype C1 = RCONST(conc1);
  realtype C2 = RCONST(conc2);
  realtype T0 = 0.;
  realtype T1 = RCONST(dt);

  y = abstol = NULL;
  A = NULL;
  LS = NULL;
  cvode_mem = NULL;
  RTOL  = RCONST(1.0e-4);   /* scalar relative tolerance            */
  ATOL1 = RCONST(1.0e-8);   /* vector absolute tolerance components */
  ATOL2 = RCONST(1.0e-8);   /* vector absolute tolerance components */

  /* Create serial vector of length NEQ for I.C. and abstol */
  y = N_VNew_Serial(NEQ);
  if (check_flag((void *)y, "N_VNew_Serial", 0)) return(1);
  abstol = N_VNew_Serial(NEQ); 
  if (check_flag((void *)abstol, "N_VNew_Serial", 0)) return(1);

  /* Initialize y */
  Ith(y,1) = C1;
  Ith(y,2) = C2;

  /* Set the scalar relative tolerance */
  reltol = RTOL;
  /* Set the vector absolute tolerance */
  Ith(abstol,1) = ATOL1;
  Ith(abstol,2) = ATOL2;

  /* Call CVodeCreate to create the solver memory and specify the 
   * Backward Differentiation Formula and the use of a Newton iteration */
  cvode_mem = CVodeCreate(CV_BDF, CV_NEWTON);
  if (check_flag((void *)cvode_mem, "CVodeCreate", 0)) return(1);
  
  /* Call CVodeInit to initialize the integrator memory and specify the
   * user's right hand side function in y'=f(t,y), the inital time T0, and
   * the initial dependent variable vector y. */
  flag = CVodeInit(cvode_mem, f1, T0, y);
  if (check_flag(&flag, "CVodeInit", 1)) return(1);

  /* Call CVodeSVtolerances to specify the scalar relative tolerance
   * and vector absolute tolerances */
  flag = CVodeSVtolerances(cvode_mem, reltol, abstol);
  if (check_flag(&flag, "CVodeSVtolerances", 1)) return(1);

  /* Create dense SUNMatrix for use in linear solves */
  A = SUNDenseMatrix(NEQ, NEQ);
  if(check_flag((void *)A, "SUNDenseMatrix", 0)) return(1);

  /* Create dense SUNLinearSolver object for use by CVode */
  LS = SUNDenseLinearSolver(y, A);
  if(check_flag((void *)LS, "SUNDenseLinearSolver", 0)) return(1);

  /* Call CVDlsSetLinearSolver to attach the matrix and linear solver to CVode */
  flag = CVDlsSetLinearSolver(cvode_mem, LS, A);
  if(check_flag(&flag, "CVDlsSetLinearSolver", 1)) return(1);

  /* Set the user-supplied Jacobian routine Jac */
  flag = CVDlsSetJacFn(cvode_mem, Jac1);
  if(check_flag(&flag, "CVDlsSetJacFn", 1)) return(1);

  /* In loop, call CVode, print results, and test for error.
     Break out of loop when NOUT preset output times have been reached.  */
  // printf(" \n3-species kinetics problem\n\n");

  tout = T1;
  // while(1) {
    // std::cout << Ith(y,1) << " ," << Ith(y,2) << std::endl;
    flag = CVode(cvode_mem, tout, y, &t, CV_NORMAL);
    // if (abs(t-tout) > 1e-8) {
    //     std::cout << "WARNING: CVODE output time (t = " << t << ") is not equal to timestep (tout = " << tout << ")" << std::endl;
    // }
    // PrintOutput(t, Ith(y,1), Ith(y,2));

    // if (check_flag(&flag, "CVode", 1)) break;
    conc1 = Ith(y,1) - C1;
    conc2 = Ith(y,2) - C2;
    // if (flag == CV_SUCCESS) {
    //   iout++;
    //   tout += 1e10;
    // }

    // if (iout == NOUT) break;

  // }

  /* Print some final statistics */
  // PrintFinalStats(cvode_mem);

  /* Free y and abstol vectors */
  N_VDestroy(y);
  N_VDestroy(abstol);

  /* Free integrator memory */
  CVodeFree(&cvode_mem);

  /* Free the linear solver memory */
  SUNLinSolFree(LS);

  /* Free the matrix memory */
  SUNMatDestroy(A);
  return(flag);
}
